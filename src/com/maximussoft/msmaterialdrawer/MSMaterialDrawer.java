package com.maximussoft.msmaterialdrawer;

import com.mikepenz.materialdrawer.Drawer;

import android.graphics.drawable.Drawable;
import android.view.View;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.collections.List;

@BA.ShortName("MSMaterialDrawer")
public class MSMaterialDrawer extends AbsObjectWrapper<Drawer.Result> {

	public void OpenDrawer(){
		getObject().openDrawer();
	}
	public void CloseDrawer(){
		getObject().closeDrawer();
	}
	public boolean IsDrawerOpen(){
		return getObject().isDrawerOpen();
	}
	
	public View GetHeader(){
		return getObject().getHeader();
	}

	public View GetFooter(){
		return getObject().getFooter();
	}
	
//	public List GetDrawerItems(){
//		return getObject().getDrawerItems();
//	}

	public void setSelection(int position){
		getObject().setSelection(position);
	}

	public void setSelectionByIdentifier(int identifier){
		getObject().setSelectionByIdentifier(identifier);
	}
	
	public int getSelection(){
		return getObject().getCurrentSelection();
	}
	
	public void updateBadge(int position, String badge){
		getObject().updateBadge(badge, position);
	}

	public void updateIcon(int position, Drawable icon){
		getObject().updateIcon(icon, position);
	}
	
	public void updateName(int position, String name){
		getObject().updateName(name, position);
	}
	
	public int getPositionFromIdentifier(int identifier){
		return getObject().getPositionFromIdentifier(identifier);
	}

}
